export interface Submenu {

  id?: number
  name?: string
  link?: string

}
