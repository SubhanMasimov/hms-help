import {Submenu} from "./submenu";

export interface Menu {

  id?: number
  name?: string
  submenuList?: Submenu[]

}
