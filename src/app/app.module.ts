import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {MainComponent} from './components/main/main.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {NavbarComponent} from './components/navbar/navbar.component';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {HomeOverviewComponent} from './pages/home/home-overview/home-overview.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import { HomeReportsComponent } from './pages/home/home-reports/home-reports.component';
import { HomeUpdatesComponent } from './pages/home/home-updates/home-updates.component';
import { DashboardWeeklyComponent } from './pages/dashboard/dashboard-weekly/dashboard-weekly.component';
import { ReportHelloComponent } from './pages/report/report-hello/report-hello.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SidebarComponent,
    NavbarComponent,
    HomeOverviewComponent,
    HomeReportsComponent,
    HomeUpdatesComponent,
    DashboardWeeklyComponent,
    ReportHelloComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

