export const getUrlParams = () => {
  let params = {};
  window.location.href.replace(
    /[?&]+([^=&]+)=([^&]*)/gi,
    (_, key, value) => (params[key] = value)
  );
  return params;
};
