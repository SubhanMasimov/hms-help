import {Component, OnInit} from '@angular/core';
import {Menu} from '../../models/menu';
import {menuList} from '../../constants/menuList';
import {getUrlParams} from '../../core/getUrlParams';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  changeableMenuList: Menu[] = [];

  constructor() {
  }

  ngOnInit() {
    this.getMenuList();
    this.createSidebarMenu();
  }

  getMenuList() {
    const params: any = getUrlParams();
    if (!params['items']) {
      return;
    }

    const items: string = params.items;
    this.changeableMenuList = [];
    const menuListString = items.split(',');
    for (let i = 0; i < menuListString.length; i++) {
      const tempMenuArray = menuListString[i].split('-');
      const tempMenu = this.getMenuById(Number(tempMenuArray[0]));
      if (tempMenuArray.length === 1) {
        this.changeableMenuList.push(tempMenu);
        continue;
      }

      this.changeableMenuList.push({
        id: tempMenu.id,
        name: tempMenu.name,
        submenuList: []
      });


      const index = this.changeableMenuList.length - 1;
      this.changeableMenuList[index].submenuList = [];

      for (let j = 1; j < tempMenuArray.length; j++) {
        const submenu = this.getSubmenuBySubmenuId(Number(tempMenuArray[j]));
        this.changeableMenuList[index].submenuList.push(submenu);
      }
    }
  }

  getMenuById(id: number) {
    return menuList.filter(x => x.id === id)[0];
  }

  getSubmenuBySubmenuId(submenuId: number) {
    for (let i = 0; i < menuList.length; i++) {
      for (let j = 0; j < menuList[i].submenuList.length; j++) {
        if (menuList[i].submenuList[j].id === submenuId) {
          return menuList[i].submenuList[j];
        }
      }
    }
  }

  createSidebarMenu() {
    const menuList = document.getElementById('list-menu');
    for (let i = 0; i < this.changeableMenuList.length; i++) {
      const id = this.changeableMenuList[i].id;
      const li = document.createElement('li');
      li.id = id.toString();
      li.classList.add('mb-1');
      menuList.appendChild(li);
      const button = document.createElement('button');
      button.addEventListener('click', () => {
        this.expandOrCollapseMenu(id);
      });
      button.innerHTML += this.changeableMenuList[i].name;
      button.classList.add('btn');
      button.classList.add('btn-toggle');
      button.classList.add('align-items-center');
      button.classList.add('rounded');
      button.classList.add('collapsed');
      button.setAttribute('aria-expanded', 'false');
      button.setAttribute('_ngcontent-c3', '');
      li.appendChild(button);
      const div = document.createElement('div');
      div.classList.add('collapse');
      li.appendChild(div);
      const ul = document.createElement('ul');
      ul.classList.add('btn-toggle-nav');
      ul.classList.add('list-unstyled');
      ul.classList.add('fw-normal');
      ul.classList.add('pb-1');
      div.appendChild(ul);

      for (let j = 0; j < this.changeableMenuList[i].submenuList.length; j++) {
        const li = document.createElement('li');
        const a = document.createElement('a');
        li.appendChild(a);
        a.href = this.createLink(this.changeableMenuList[i].submenuList[j].link);
        a.innerHTML += this.changeableMenuList[i].submenuList[j].name;
        a.classList.add('list-group-item');
        a.classList.add('border-0');
        a.classList.add('ml-3');
        a.classList.add('text-decoration-underline');
        a.classList.add('text-secondary');
        a.classList.add('py-0');
        a.classList.add('my-1');
        ul.appendChild(li);
      }
    }
  }

  expandOrCollapseMenu(menuId: number) {
    const menu = document.getElementById(menuId.toString());
    const button = menu.children[0];
    const div = menu.children[1];
    if (div.classList.contains('show')) {
      div.classList.remove('show');
      button.setAttribute('aria-expanded', 'false');
      return;
    }

    div.classList.add('show');
    button.setAttribute('aria-expanded', 'true');
  }

  createLink(submenuLink: string) {
    let url = '/#/' + submenuLink + '?';
    const params = getUrlParams();
    for (let param in params) {
      url += `${param}=${params[param]}&`;
    }
    return url;
  }

}
