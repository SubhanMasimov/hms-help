import {Component, OnInit} from '@angular/core';
import {getUrlParams} from '../../core/getUrlParams';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  isSidebarShow: boolean = false;

  constructor() {
  }

  ngOnInit() {
    this.checkRouteForSidebarShow();
  }

  checkRouteForSidebarShow() {
    let params = getUrlParams();

    if (params['sidebar'] && params['sidebar'] === 'true') {
      this.isSidebarShow = true;
      return;
    }

    if (params['sidebar'] && params['sidebar'] === 'false') {
      this.isSidebarShow = false;
      return;
    }

  }

  getClassForMainContent() {
    return this.isSidebarShow === true ? 'col-md-10 px-5 py-3' : 'col-md-12 px-5 py-3';
  }

}
