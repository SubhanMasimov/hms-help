import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeOverviewComponent} from './pages/home/home-overview/home-overview.component';
import {HomeReportsComponent} from './pages/home/home-reports/home-reports.component';
import {HomeUpdatesComponent} from './pages/home/home-updates/home-updates.component';
import {DashboardWeeklyComponent} from './pages/dashboard/dashboard-weekly/dashboard-weekly.component';
import {ReportHelloComponent} from './pages/report/report-hello/report-hello.component';

export const routes: Routes = [
  {path: 'home/overview', component: HomeOverviewComponent},
  {path: 'home/updates', component: HomeUpdatesComponent},
  {path: 'home/reports', component: HomeReportsComponent},
  {path: 'dashboard/weekly', component: DashboardWeeklyComponent},
  {path: 'report/hello', component: ReportHelloComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
