import {Menu} from "../models/menu";

export const menuList: Menu[] = [
  {
    id: 1,
    name: 'Qeydiyyat',
    submenuList: [
      {id: 1001, name: 'Overview', link: '/home/overview'},
      {id: 1002, name: 'Updates', link: '/home/updates'},
      {id: 1003, name: 'Reports', link: '/home/reports'}
    ]
  },
  {
    id: 2,
    name: 'Kassa',
    submenuList: [
      {id: 2001, name: 'Weekly', link: '/dashboard/weekly'}
    ]
  },
  {
    id: 3,
    name: 'Laboratoriya',
    submenuList: [
      {id: 3001, name: 'Hello', link: '/report/hello'}
    ]
  },
  {
    id: 4,
    name: 'Həkim otağı',
    submenuList: [
      {id: 3001, name: 'Hello', link: '/report/hello'}
    ]
  },
  {
    id: 5,
    name: 'Anbar',
    submenuList: [
      {id: 3001, name: 'Hello', link: '/report/hello'}
    ]
  },
  {
    id: 6,
    name: 'Mühasibatlıq',
    submenuList: [
      {id: 3001, name: 'Hello', link: '/report/hello'}
    ]
  },
  {
    id: 7,
    name: 'Administrator',
    submenuList: [
      {id: 3001, name: 'Hello', link: '/report/hello'}
    ]
  },
  {
    id: 8,
    name: 'Ümumi',
    submenuList: [
      {id: 3001, name: 'Hello', link: '/report/hello'}
    ]
  },
  {
    id: 9,
    name: 'Kadrlar',
    submenuList: [
      {id: 3001, name: 'Hello', link: '/report/hello'}
    ]
  },
]
