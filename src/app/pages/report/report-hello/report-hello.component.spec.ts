import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportHelloComponent } from './report-hello.component';

describe('ReportHelloComponent', () => {
  let component: ReportHelloComponent;
  let fixture: ComponentFixture<ReportHelloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportHelloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportHelloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
